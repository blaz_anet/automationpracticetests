*** Settings ***
Library           Selenium2Library

*** Test Cases ***
test
   Open Browser    https://www.google.com    chrome
   Page Should Contain Element    xpath=//div[@id="hplogo"]
   Input Text    xpath=//input[@id="lst-ib"]    VSE
   Press Key    xpath=//input[@id="lst-ib"]    \\13
   Page Should Contain Link    xpath=//a[@data-href='https:\/\/www.vse.cz\/']
   Click Link    xpath=//a[@data-href='https:\/\/www.vse.cz\/']
   Page Should Contain Image    xpath=//img[@id="logo"]

